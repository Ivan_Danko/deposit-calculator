﻿using Caliburn.Micro;
using DCalc.Constants;
using DCalc.Helpers;
using DCalc.Models;
using System;

namespace DCalc.ViewModels
{
    public class ShellViewModel : Screen
    {
        private uint _amount = CalcConstants.DefaultAmount;
        private uint _period = CalcConstants.DefaultPeriod;
        private bool _isCapitalized = true;
        private BindableCollection<CurrencyModel> _currencies = new BindableCollection<CurrencyModel>();
        private CurrencyModel _selectedCurruency = new CurrencyModel { Currency = "$", Interest = "3%" };

        public ShellViewModel()
        {
            Currencies.Add(new CurrencyModel { Currency = "$", Interest = "3%" });
            Currencies.Add(new CurrencyModel { Currency = "€", Interest = "4%" });
            Currencies.Add(new CurrencyModel { Currency = "₴", Interest = "16%" });
        }
        public uint Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                if (value > CalcConstants.MaxAmount) 
                {
                    value = CalcConstants.MaxAmount; 
                } 
                else if (value < CalcConstants.MinAmount) 
                {
                    value = CalcConstants.MinAmount; 
                }
                _amount = value;
                NotifyOfPropertyChange(() => Total);
            }
        }

        public uint Period
        {
            get
            {
                return _period;
            }
            set
            {
                if (value > CalcConstants.MaxPeriod)
                {
                    value = CalcConstants.MaxPeriod;
                }
                else if (value < CalcConstants.MinPeriod)
                {
                    value = CalcConstants.MinPeriod;
                }
                _period = value;
                NotifyOfPropertyChange(() => Total);
            }
        }

        public BindableCollection<CurrencyModel> Currencies
        {
            get { return _currencies; }
            set { _currencies = value; }
        }

        public CurrencyModel SelectedCurrency
        {
            get { return _selectedCurruency; }
            set { _selectedCurruency = value; NotifyOfPropertyChange(() => Total); NotifyOfPropertyChange(() => SelectedCurrency); }
        }

        public double Interest
        {
            get { return CurrencyInterestEnumHelper.GetCurrencyInterest(_selectedCurruency?.Currency); }
        }

        public bool IsCapitalized
        {
            get { return _isCapitalized; }
            set
            {
                _isCapitalized = value;
                NotifyOfPropertyChange(() => Total);
            }
        }

        public string Total
        {
            get { return IsCapitalized ? (Amount * Math.Pow(1 + Interest / 12, Period)).ToString("#.##") : (Amount * (1 + Interest / 12 * Period)).ToString("#.##"); }
        }
    }
}
