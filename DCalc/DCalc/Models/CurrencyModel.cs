﻿namespace DCalc.Models
{
    public class CurrencyModel
    {
        public string? Currency { get; set; }
        public string? Interest { get; set; }
    }
}
