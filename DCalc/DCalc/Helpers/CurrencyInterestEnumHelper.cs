﻿using DCalc.Constants;

namespace DCalc.Helpers
{
    public class CurrencyInterestEnumHelper
    {
        public static double GetCurrencyInterest(string currency)
        {
            switch (currency)
            {
                case "$": return CalcConstants.USDInterest;
                case "€": return CalcConstants.EURInterest;
                case "₴": return CalcConstants.UAHInterest;
                default: return CalcConstants.USDInterest;
            }
        }
    }
}
