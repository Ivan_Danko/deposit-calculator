﻿namespace DCalc.Constants
{
    internal static class CalcConstants
    {
        internal static uint MaxAmount = 100000000;
        internal static uint MinAmount = 100;
        internal static uint MaxPeriod = 36;
        internal static uint MinPeriod = 3;
        internal static uint DefaultPeriod = 12;
        internal static uint DefaultAmount = 10000;
        internal static double USDInterest = 0.03;
        internal static double EURInterest = 0.04;
        internal static double UAHInterest = 0.16;
    }
}
